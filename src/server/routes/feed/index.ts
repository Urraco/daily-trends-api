import { Router } from "express";
import {
  addFeed,
  updateFeed,
  getFeed,
  listFeed,
  deleteFeed,
} from "../../../features";
import { IFeedInput } from "../../../types";

// TODO: Implement zod validations

const FeedRouter = Router();

FeedRouter.get("/", async (_, res) => {
  const result = await listFeed();
  res.status(result ? 200 : 400).json(result);
});

FeedRouter.get("/:id", async (req, res) => {
  const result = await getFeed(req.params.id);
  res.status(result ? 200 : 400).json(result);
});

FeedRouter.post("/", async (req, res) => {
  let result;
  const { title } = req.body;
  if (title) {
    const input: IFeedInput = {
      title,
      source: "manual",
    };

    result = await addFeed(input);
  }
  res.status(result ? 200 : 400).json(result);
});

FeedRouter.put("/:id", async (req, res) => {
  let result;
  const { title } = req.body;
  if (title) {
    const input: IFeedInput = {
      title,
      source: "manual",
    };
    result = await updateFeed(req.params.id, input);
  }
  res.status(result ? 200 : 400).json(result);
});

FeedRouter.delete("/:id", async (req, res) => {
  const result = await deleteFeed(req.params.id);
  res.status(result ? 200 : 400).json(result);
});

export { FeedRouter };
