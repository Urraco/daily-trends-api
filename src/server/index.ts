import express from "express";
import { FeedRouter } from "./routes/feed";
import config from "../config";

export const initializeApplication = () => {
  const app = express();

  app.listen(config.api.port);

  app.use(express.json());
  app.use("/feeds", FeedRouter);

  return app;
};
