import mongoose from "mongoose";
import { fetchFeeds } from "./services";
import { mongoConnection } from "./config/database";

async function main() {
  await mongoConnection();
  await fetchFeeds();
}

main().then(() => process.exit(0));
