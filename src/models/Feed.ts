import { model, Schema } from "mongoose";
import { IFeedDocument } from "../types";

const FeedSchema = new Schema({
  title: { type: String, required: true },
  body: String,
  author: String,
  source: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: null },
});

const Feed = model<IFeedDocument>("Feed", FeedSchema);

export { Feed };
