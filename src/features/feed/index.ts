import { Feed } from "../../models";
import { IFeedInput } from "../../types";

const addFeed = async (input: IFeedInput) => {
  const feed = new Feed({
    title: input.title,
    source: "manual",
  });

  const result = await feed.save();

  return result;
};

const addManyFeeds = async (input: IFeedInput[]) => {
  const result = await Feed.insertMany(input);

  return result;
};

const getFeed = async (id: string) => {
  const result = await Feed.findById(id);

  return result;
};

const updateFeed = async (id: string, input: IFeedInput) => {
  const result = await Feed.findByIdAndUpdate(id, {
    title: input.title,
    updatedAt: new Date(),
  });

  return result;
};

const deleteFeed = async (id: string) => {
  const result = await Feed.deleteOne({ id });

  return result;
};

const listFeed = async () => {
  const result = await Feed.find().sort([["createdAt", -1]]);

  return result;
};

export { addFeed, addManyFeeds, updateFeed, getFeed, listFeed, deleteFeed };
