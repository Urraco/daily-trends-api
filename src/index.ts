import { fetchFeeds } from "./services";
import config from "./config";
import { initializeApplication } from "./server";
import { mongoConnection } from "./config/database";

async function main() {
  await mongoConnection();
  initializeApplication();

  await fetchFeeds();
}

main()
  .then(() => console.log(`Service started at ${config.api.port}!`))
  .catch((e) => console.error(e));
