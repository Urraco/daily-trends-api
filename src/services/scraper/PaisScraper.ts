import { IFeedInput } from "feed";
import { Scraper } from "./Scraper";

class PaisScraper extends Scraper {
  protected url = "https://elpais.com";
  protected sectionSelector = `//*[@id="fusion-app"]/main/div[1]/section`;
  protected source = "ElPais";

  public async getFeeds() {
    await this.init();
    let articles: IFeedInput[] = [];

    if (this.browser) {
      const page = await this.browser.newPage();

      await page.goto(this.url);

      await page.waitForXPath(this.sectionSelector);

      const handleSection = await page.$x(this.sectionSelector);

      articles = await page.evaluate((section) => {
        const headers = section.parentElement?.querySelectorAll("h2");
        const textHeaders: IFeedInput[] = [];
        headers?.forEach((h) => {
          const title = h.innerText;
          if (title && textHeaders.length < 5) {
            textHeaders.push({ title, source: "elpais" });
          }
        });
        return textHeaders;
      }, handleSection[0]);
    }

    await this.close();
    return articles;
  }
}

export { PaisScraper };
