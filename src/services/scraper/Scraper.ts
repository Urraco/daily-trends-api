import puppeteer, { Browser } from "puppeteer";
import { IFeedInput } from "../../types";

interface IScraper {
  getFeeds: () => Promise<IFeedInput[]>;
}

abstract class Scraper implements IScraper {
  protected browser?: Browser;
  abstract getFeeds(): Promise<IFeedInput[]>;

  protected async init() {
    this.browser = await puppeteer.launch();
  }

  protected async close() {
    if (this.browser) {
      await this.browser.close();
    }
  }
}

export { Scraper };
