import { IFeedInput } from "feed";
import { Scraper } from "./Scraper";

class MundoScraper extends Scraper {
  protected url = "https://elmundo.es";
  protected sectionSelector = `//*[@id="js_526feae561fd3d3c3f8b4578"]/div[6]/div/div/div/div`;
  protected source = "ElMundo";

  public async getFeeds() {
    await this.init();
    let articles: IFeedInput[] = [];

    if (this.browser) {
      const page = await this.browser.newPage();

      await page.goto(this.url);

      await page.waitForXPath(this.sectionSelector);

      const handleSection = await page.$x(this.sectionSelector);

      articles = await page.evaluate((section) => {
        const headers = section.parentElement?.querySelectorAll("h2");
        const textHeaders: IFeedInput[] = [];
        headers?.forEach((h) => {
          const title = h.innerText;
          if (title && textHeaders.length < 5) {
            textHeaders.push({ title, source: "elmundo" });
          }
        });
        return textHeaders;
      }, handleSection[0]);
    }

    await this.close();
    return articles;
  }
}

export { MundoScraper };
