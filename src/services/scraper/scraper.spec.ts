import { MundoScraper } from "./MundoScraper";
import { PaisScraper } from "./PaisScraper";

describe("Scraper tests", () => {
  it("fetch feed from ElMundo", async () => {
    const scraper = new MundoScraper();

    const articles = await scraper.getFeeds();

    expect(articles).toHaveLength(5);
  });

  it("fetch feed from ElPais", async () => {
    const scraper = new PaisScraper();

    const articles = await scraper.getFeeds();

    expect(articles).toHaveLength(4);
  });
});
