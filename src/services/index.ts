import { addManyFeeds } from "../features";
import { MundoScraper } from "./scraper/MundoScraper";
import { PaisScraper } from "./scraper/PaisScraper";

const fetchFeeds = async () => {
  // TODO: Retrieve more info and avoid duplicity of news

  const mundoScraper = new MundoScraper();
  const paisScraper = new PaisScraper();

  const promises = [mundoScraper.getFeeds(), paisScraper.getFeeds()];

  const articles = await Promise.all(promises);

  await addManyFeeds(articles.flat());
};

export { fetchFeeds };
