import { IConfig } from "../types/config";

const config: IConfig = {
  api: {
    port: Number(process.env.API_PORT ?? 3000),
  },
  mongoUri: process.env.MONGODB_URI ?? "mongodb://localhost:27017/dt",
};

export default config;
