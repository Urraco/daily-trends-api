import mongoose from "mongoose";
import config from "./index";

const mongoConnection = async () => {
  await mongoose.connect(config.mongoUri);
  const database = mongoose.connection;

  database.on("error", (error) => {
    console.log(error);
  });

  database.once("connected", () => {
    console.log("Database Connected");
  });
};

export { mongoConnection };
