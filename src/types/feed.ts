import { Document } from "mongoose";

interface IFeedDocument extends Document {
  id: string;
  title: string;
  body: string;
  author: string;
  source: "manual" | "elpais" | "elmundo";
  createdAt: Date;
  updatedAt: Date;
}

interface IFeedInput {
  title: string;
  body?: string;
  author?: string;
  source: string;
}

export { IFeedDocument, IFeedInput };
