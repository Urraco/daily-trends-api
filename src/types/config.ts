interface IConfig {
  api: {
    port: number;
  };
  mongoUri: string;
}

export { IConfig };
