# DailyTrendsAPI

This service holds a REST API to handle news feeds.

---

## API Development

Env variables needed for this project

```
API_PORT=
MONGODB_URI=
```

To run this project use `yarn develop`

---

## Scraping Service Job

This will fetch news from ElMundo and ElPais, storing them in MongoDB

```
MONGODB_URI=
```

To run this project use `yarn develop:scrape`

---

## Folder Structure

```
src
 |-config
 |
 |-features         Database operations, unit testing inside
 |
 |-models           Business model
 |
 |-server           Express API Routes
 |
 |-services         Service logic
 |
 |-test             Integration testing
 |
 |-types            Typescript interfaces


```

---

### TO BE IMPLEMENTED

- Improve scraping service, iterate DOM to retrieve author, body and some identifier
- Store news based on identifier to avoid duplicity
- Validate request params in express
- Test CRUD
